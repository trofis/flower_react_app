import { Outlet, useParams } from 'react-router-dom';

function Contact() {
    const { flowerName } = useParams();
    return <h3>{flowerName}</h3>;
}

export default Contact;
