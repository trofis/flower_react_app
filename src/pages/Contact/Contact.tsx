import { Outlet } from 'react-router-dom';

function Contact() {
    return (
        <>
            <h3>My form</h3>
            <Outlet />
        </>
    );
}

export default Contact;
