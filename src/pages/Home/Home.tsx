import Grid from '@mui/material/Grid';

import { useState } from 'react';

import { Item } from '../../types/Item';

import './Home.css';

// Comonents
import Cart from '../../components/Cart/Cart';
import ListItems from '../../components/ListItems/ListItems';
import Contact from '../../components/ContactForm/ContactForm';

function Home() {
    const [basket, setBasket] = useState<Item[]>([]);
    const [total, setTotal] = useState<number>(0);

    return (
        <Grid
            container
            direction="row"
            justifyContent="flex-start"
            alignItems="flex-start"
        >
            <Grid item xs={4}>
                <Cart
                    basket={basket}
                    total={total}
                    setBasket={setBasket}
                    setTotal={setTotal}
                />
            </Grid>
            <Grid item xs={8} spacing={4}>
                <ListItems
                    basket={basket}
                    total={total}
                    setBasket={setBasket}
                    setTotal={setTotal}
                />
                <Contact />
            </Grid>
        </Grid>
    );
}

export default Home;
