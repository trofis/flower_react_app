import React from 'react';
import ReactDOM from 'react-dom/client';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';

import './index.css';

import Contact from './pages/Contact/Contact';
import Flower from './pages/Flower/Flower';

import Header from './components/Header/Header';
import ContactForm from './components/ContactForm/ContactForm';
import Home from './pages/Home/Home';
import Error from './Error';

ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(
    <React.StrictMode>
        <Router>
            <Header />
            <Routes>
                <Route path="/" element={<Home />} />
                <Route path="/flower/:flowerName" element={<Flower />} />

                <Route path="/contact" element={<Contact />}>
                    <Route path="default" element={<ContactForm />} />
                </Route>
                <Route path="*" element={<Error />} />
            </Routes>
        </Router>
    </React.StrictMode>
);
