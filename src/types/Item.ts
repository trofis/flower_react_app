export type Item = {
    name: string;
    price: number;
    img: string;
};
