import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemText from '@mui/material/ListItemText';
import Grid from '@mui/material/Grid';

import { useState } from 'react';

import './Cart.css';

import { Item } from '../../types/Item';

import CardItem from '../CardItem/CardItem.tsx';

function Cart({ basket, total, setBasket, setTotal }) {
    const removeBasket = (item: Item) => {
        console.log(`Add basket: ${item.name}`);

        setBasket(basket.filter((b: Item) => b !== item));
        setTotal(total - item.price);
    };

    const listBasket = basket.map((item: Item) => (
        <CardItem item={item} process={removeBasket} />
    ));

    return (
        <Grid
            container
            direction="column"
            justifyContent="space-around"
            alignItems="flex-start"
        >
            <h2>My basket</h2>
            <List>
                <ListItem disablePadding>
                    {listBasket.length > 0 ? (
                        listBasket
                    ) : (
                        <p>Your cart is empty</p>
                    )}
                </ListItem>
            </List>
            <h3>
                Total
                {total}
            </h3>
        </Grid>
    );
}

export default Cart;
