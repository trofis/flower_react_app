import TextField from '@mui/material/TextField';
import Grid from '@mui/material/Grid';
import Button from '@mui/material/Button';
import SendIcon from '@mui/icons-material/Send';
import Alert from '@mui/material/Alert';
import AlertTitle from '@mui/material/AlertTitle';

import { useState, useEffect } from 'react';

function Cart() {
    const [isClicked, setIsClicked] = useState<boolean>(false);
    const [name, setName] = useState<string>('');
    const [mail, setMail] = useState<string>('');
    const [text, setText] = useState<string>('');

    useEffect(() => {
        document.title = `Customer ${name}`;
        console.log('Clicked :', isClicked);
    }, [isClicked, name]); // When is clicked change,
    // we can have an empty list it will be performed only at the beginning

    const onSubmit = (e: any) => {
        e.preventDefault();
        setIsClicked(true);
        const timeout = setTimeout(() => {
            setIsClicked(false);
            clearInterval(timeout);
        }, 3000);
    };

    return (
        <form onSubmit={onSubmit}>
            <Grid
                container
                direction="column"
                alignItems="flex-start"
                spacing={2}
            >
                <Grid item>
                    <TextField
                        required
                        id="name"
                        label="Name"
                        defaultValue="John"
                        value={name}
                        onChange={(e: any) => setName(e.target.value)}
                        variant="standard"
                    />
                </Grid>
                <Grid item>
                    <TextField
                        required
                        id="mail"
                        label="Email Address"
                        defaultValue="john@gmail.com"
                        value={mail}
                        onChange={(e: any) => setMail(e.target.value)}
                        variant="standard"
                    />
                </Grid>
                <Grid item>
                    <TextField
                        required
                        id="mail"
                        label="Your message"
                        multiline
                        value={text}
                        rows={4}
                        onChange={(e: any) => {
                            setText(e.target.value);
                            console.log(text);
                        }}
                    />
                </Grid>
                <Grid item>
                    <Button
                        type="submit"
                        variant="contained"
                        endIcon={<SendIcon />}
                    >
                        Send
                    </Button>
                </Grid>

                <Grid item>
                    {isClicked && (
                        <Alert severity="success">
                            <AlertTitle>Success</AlertTitle>
                            Message sent successfully :{text}
                        </Alert>
                    )}
                </Grid>
            </Grid>
        </form>
    );
}

export default Cart;
