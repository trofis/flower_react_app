import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';

import { Item } from '../../types/Item';

import CardItem from '../CardItem/CardItem.tsx';

function ListItems({ basket, total, setBasket, setTotal }) {
    const items: Item[] = [
        { name: 'Monstera', price: 8, img: '../../../public/monstera.jpg' },
        { name: 'Lierre', price: 10, img: '../../../public/lierre.avif' },
        { name: 'Bouquet', price: 15, img: '../../../public/bouquet.jpg' },
    ];

    const addBasket = (item: Item) => {
        console.log(`Add basket: ${item.name}`);
        basket.push(item);
        setTotal(item.price + total);
    };

    // const listItems = items.map(item =>
    //     <CardItem item={item} process={addBasket} />
    // );

    return (
        <>
            <h2>Shop items</h2>
            <List>
                <ListItem disablePadding>
                    {items.map((item: Item) => (
                        <CardItem item={item} process={addBasket} />
                    ))}
                </ListItem>
            </List>
        </>
    );
}

export default ListItems;
