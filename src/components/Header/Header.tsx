import Stack from '@mui/material/Stack';
import Grid from '@mui/material/Grid';

import { Link } from 'react-router-dom';
import Leaf from '../../assets/feuille.png';

import './Header.css';

function Header() {
    const title = 'La maison jungle';
    return (
        <Grid
            container
            direction="row"
            justifyContent="flex-end"
            alignItems="center"
            spacing={4}
        >
            <Grid item>
                <Link className="subLink" to="/contact/default">
                    <h2>Contacts</h2>
                </Link>
            </Grid>
            <Grid item>
                <Link className="Home" to="/">
                    <Stack direction="row" alignItems="center" spacing={2}>
                        <h1>{title}</h1>
                        <a href="/">
                            <img src={Leaf} alt="" />
                        </a>
                    </Stack>
                </Link>
            </Grid>
        </Grid>
    );
}

export default Header;
