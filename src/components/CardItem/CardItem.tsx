import ListItemButton from '@mui/material/ListItemButton';
import ListItemText from '@mui/material/ListItemText';

function CardItem({ item, process }) {
    const priceText = (price: number) => `${price} €`;

    return (
        <ListItemButton onClick={() => process(item)}>
            <img src={item.img} alt="" />
            <ListItemText
                primary={item.name}
                secondary={priceText(item.price)}
            />
        </ListItemButton>
    );
}

export default CardItem;
